
package fr.astek.events;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Fields implements Serializable
{

    private String lieuAdresse2;
    private String descriptifLong;
    private Float googlemapLongitude;
    private String descriptifCourt;
    private String manifestationGratuite;
    private String themeDeLaManifestation;
    private String categorieDeLaManifestation;
    private String commune;
    private String horaires;
    private String dateFin;
    private String datesAffichageHoraires;
    private List<Float> geoPoint = null;
    private Float googlemapLatitude;
    private String typeDeManifestation;
    private String reservationSiteInternet;
    private String stationMetroTramAProximite;
    private String lieuNom;
    private String nomDeLaManifestation;
    private String identifiant;
    private String dateDebut;
    private Integer codePostal;

    public String getLieuAdresse2() {
        return lieuAdresse2;
    }

    public void setLieuAdresse2(String lieuAdresse2) {
        this.lieuAdresse2 = lieuAdresse2;
    }

    public String getDescriptifLong() {
        return descriptifLong;
    }

    public void setDescriptifLong(String descriptifLong) {
        this.descriptifLong = descriptifLong;
    }

    public Float getGooglemapLongitude() {
        return googlemapLongitude;
    }

    public void setGooglemapLongitude(Float googlemapLongitude) {
        this.googlemapLongitude = googlemapLongitude;
    }

    public String getDescriptifCourt() {
        return descriptifCourt;
    }

    public void setDescriptifCourt(String descriptifCourt) {
        this.descriptifCourt = descriptifCourt;
    }

    public String getManifestationGratuite() {
        return manifestationGratuite;
    }

    public void setManifestationGratuite(String manifestationGratuite) {
        this.manifestationGratuite = manifestationGratuite;
    }

    public String getThemeDeLaManifestation() {
        return themeDeLaManifestation;
    }

    public void setThemeDeLaManifestation(String themeDeLaManifestation) {
        this.themeDeLaManifestation = themeDeLaManifestation;
    }

    public String getCategorieDeLaManifestation() {
        return categorieDeLaManifestation;
    }

    public void setCategorieDeLaManifestation(String categorieDeLaManifestation) {
        this.categorieDeLaManifestation = categorieDeLaManifestation;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getHoraires() {
        return horaires;
    }

    public void setHoraires(String horaires) {
        this.horaires = horaires;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public String getDatesAffichageHoraires() {
        return datesAffichageHoraires;
    }

    public void setDatesAffichageHoraires(String datesAffichageHoraires) {
        this.datesAffichageHoraires = datesAffichageHoraires;
    }

    public List<Float> getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(List<Float> geoPoint) {
        this.geoPoint = geoPoint;
    }

    public Float getGooglemapLatitude() {
        return googlemapLatitude;
    }

    public void setGooglemapLatitude(Float googlemapLatitude) {
        this.googlemapLatitude = googlemapLatitude;
    }

    public String getTypeDeManifestation() {
        return typeDeManifestation;
    }

    public void setTypeDeManifestation(String typeDeManifestation) {
        this.typeDeManifestation = typeDeManifestation;
    }

    public String getReservationSiteInternet() {
        return reservationSiteInternet;
    }

    public void setReservationSiteInternet(String reservationSiteInternet) {
        this.reservationSiteInternet = reservationSiteInternet;
    }

    public String getStationMetroTramAProximite() {
        return stationMetroTramAProximite;
    }

    public void setStationMetroTramAProximite(String stationMetroTramAProximite) {
        this.stationMetroTramAProximite = stationMetroTramAProximite;
    }

    public String getLieuNom() {
        return lieuNom;
    }

    public void setLieuNom(String lieuNom) {
        this.lieuNom = lieuNom;
    }

    public String getNomDeLaManifestation() {
        return nomDeLaManifestation;
    }

    public void setNomDeLaManifestation(String nomDeLaManifestation) {
        this.nomDeLaManifestation = nomDeLaManifestation;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Integer getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(Integer codePostal) {
        this.codePostal = codePostal;
    }
}
