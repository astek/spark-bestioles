
package fr.astek.events;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Event implements Serializable
{

    private String datasetid;
    private String recordid;
    private Fields fields;
    private Geometry geometry;
    private String recordTimestamp;

    public String getDatasetid() {
        return datasetid;
    }

    public void setDatasetid(String datasetid) {
        this.datasetid = datasetid;
    }

    public String getRecordid() {
        return recordid;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getRecordTimestamp() {
        return recordTimestamp;
    }

    public void setRecordTimestamp(String recordTimestamp) {
        this.recordTimestamp = recordTimestamp;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("datasetid", datasetid).append("recordid", recordid).append("fields", fields).append("geometry", geometry).append("recordTimestamp", recordTimestamp).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(datasetid).append(recordTimestamp).append(recordid).append(geometry).append(fields).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Event) == false) {
            return false;
        }
        Event rhs = ((Event) other);
        return new EqualsBuilder().append(datasetid, rhs.datasetid).append(recordTimestamp, rhs.recordTimestamp).append(recordid, rhs.recordid).append(geometry, rhs.geometry).append(fields, rhs.fields).isEquals();
    }

}
