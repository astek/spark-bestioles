package fr.astek.events;

import static org.apache.spark.sql.functions.col;

import java.io.File;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello Events!
 *
 * @see https://spark.apache.org/docs/2.2.0/sql-programming-guide.html
 */
public class App {

	private static final String EVENTS = "events.json";

	private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		final ClassLoader classLoader = App.class.getClassLoader();
		final File logEventsFile = new File(classLoader.getResource(EVENTS).getFile());

		// fix https://stackoverflow.com/a/35652866
		if ((System.getProperty("os.name").startsWith("Windows"))) {
			System.setProperty("hadoop.home.dir", "C:\\winutils");
		}

		final SparkSession spark = SparkSession.builder()
				.appName(APPLICATION_NAME)
				.master("local[2]")
				.getOrCreate();

		sparkSqlEvents(spark, logEventsFile);

		spark.stop();
	}

	private static final String APPLICATION_NAME = "Events Application";

	private static void sparkSqlEvents(final SparkSession spark, final File logFile) {

		// filter by lambda expression
		Encoder<Event> eventsEncoder = Encoders.bean(Event.class);
		Dataset<Event> eventDS = spark.read().json(logFile.getPath()).as(eventsEncoder);
		eventDS.printSchema();
		eventDS.filter(col("datasetid").equalTo("agenda-des-manifestations-culturelles-so-toulouse")).show();

		// filter by sql
		SQLContext sqlContext = spark.sqlContext();
		eventDS.createOrReplaceTempView("events");
		Dataset<Row> lieuAdresse2 = sqlContext.sql("SELECT fields.lieuAdresse2 FROM events LIMIT 10");
		lieuAdresse2.show();
		
		// 2017-10-05 : selection activité musique
		Dataset<Row> activiteMusique = sqlContext.sql("SELECT fields.themeDeLaManifestation, 1 as IDMusique FROM events where fields.themeDeLaManifestation like '%Musique%'");
		activiteMusique.show();
		
		// 2017-10-05 : selection activité Historique
		Dataset<Row> activiteHistorique = sqlContext.sql("SELECT fields.themeDeLaManifestation, 1 as IDHisto FROM events where fields.themeDeLaManifestation like '%Historique%'");
		activiteHistorique.show();
		
		// 2017-10-05 : join entre les 2 Datasets précédents, pb sur jointure
		activiteHistorique.join(activiteMusique , activiteMusique.col("IDMusique").equalTo(col("IDHisto")),"left")
		.show();
		
		// 2017-10-05 : selection jointure simplificqtion d'ecriture qd un objet a pls objets dedans
		// Pas possible de jointer sur un meme json ; avoir 2 json
		//Dataset<Row> activiteMusique1 = sqlContext.sql("SELECT fields.themeDeLaManifestation FROM events e  left join on e.recordid = e.recordid where fields.themeDeLaManifestation like '%Musique%'");
		// activiteMusique1.show();

		eventDS.filter(col("fields.themeDeLaManifestation").contains("Musique"))
			.select(col("fields.themeDeLaManifestation")).show();
		eventDS.filter(col("fields.themeDeLaManifestation").contains("Musique"))
			.select("fields.themeDeLaManifestation","fields.commune").show();
	
		eventDS.filter(col("fields.themeDeLaManifestation")
				.contains("Musique"))
				.union(eventDS.filter(col("fields.commune")
				.contains("TOULOUSE")))				
		.select("fields.themeDeLaManifestation","fields.commune").show();
				
	
		
		
	}
}
