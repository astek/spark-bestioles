package fr.astek.bestioles;

import java.io.File;
import org.apache.spark.api.java.JavaRDD;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.api.r.SQLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello Bestioles!
 *
 * @see https://spark.apache.org/docs/2.2.0/quick-start.html
 */
public class App {

    private static final String FILE = "bestioles.txt";
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        final ClassLoader classLoader = App.class.getClassLoader();
        final File logFile = new File(classLoader.getResource(FILE).getFile());
        
        // fix https://stackoverflow.com/a/35652866
        if((System.getProperty("os.name").startsWith("Windows"))) {
        	System.setProperty("hadoop.home.dir", "C:\\winutils");
        }
        
        final SparkSession spark = SparkSession.builder()
            .appName(APPLICATION_NAME)
            .master("local")
            .getOrCreate();

        sparkSql(spark, logFile);
        sparkContext(spark, logFile);

        spark.stop();
    }
    private static final String APPLICATION_NAME = "Bestiole Application";

    private static void sparkSql(final SparkSession spark, final File logFile) {
        final Dataset<String> logData = spark.read().textFile(logFile.getPath()).cache();

        // Afficher le nombre de lignes avec des A et le nombre de lignes avec des B
        long numAs = logData.filter(s -> s.contains("A")).count();
        long numBs = logData.filter(s -> s.contains("B")).count();

        System.out.println("Lines with a: " + numAs + ", lines with b: " + numBs);
        LOGGER.info("Lines with a: {}, lines with b: {}", numAs, numBs);
    }

    private static void sparkContext(final SparkSession spark, final File logFile) {
        final JavaRDD<String> lines = SQLUtils.getJavaSparkContext(spark).textFile(logFile.getPath());
        
        // Afficher la longueur totale de toutes les lignes
        
        final JavaRDD<Integer> lineLengths = lines.map(s -> s.length());
        int totalLength = lineLengths.reduce((a, b) -> a + b);

        System.out.printf("Total length %d.\n", totalLength);
        LOGGER.info("Total length {}", totalLength);
    }
}
